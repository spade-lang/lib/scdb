
#top = test::hierarchical_test_harness

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.rst = "true"
    await FallingEdge(clk)
    s.i.rst = "false"

    s.i.offset = "0";
    s.i.receive_next = "true"
    s.o.assert_eq("ScanchainMessage::Message(None())")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(None())")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(None())")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(None())")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(None())")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::End()")


    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(2))")
    s.i.offset = "10";
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(1))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(11))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(10))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(0))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::End()")

    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(12))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(11))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(21))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(20))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::Message(Some(10))")
    await FallingEdge(clk); s.o.assert_eq("ScanchainMessage::End()")

