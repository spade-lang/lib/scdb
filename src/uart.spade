use lib::main::Scdbg;
use lib::main::ScanchainMessage;
use lib::main::controller;
use protocols::uart::uart_tx;

enum State {
    SwitchNext,
    SendByte{byte_mask: uint<4>, word: uint<32>},
    SendNewline
}

// For now, assuming sizeof(T) <= 31
entity scdbg_uart<T, #SizeofT>(
    clk: clock,
    rst: bool,
    loopback: Scdbg<T>,
    uart_config: protocols::uart::UartConfig,
    tx: &mut bool,
) -> Scdbg<T> {
    decl uart_ready;
    decl state;

    let receive_next = match state {
        State::SwitchNext => true,
        _ => false,
    };

    let (message, scanchain) = inst controller$(clk, rst, receive_next, loopback);

    reg(clk) state reset(rst: State::SwitchNext) = match state {
        State::SwitchNext => {
            match *message {
                ScanchainMessage::End => State::SwitchNext,
                ScanchainMessage::Message(None) => State::SwitchNext,
                ScanchainMessage::Message(Some(val)) => {
                    // This name will appear in the code with this unique name
                    // which we can look for in the client
                    let look_for_this = val;
                    let casted: uint<SizeofT> = std::conv::unsafe::unsafe_cast(val);
                    State::SendByte$(
                        byte_mask: 0b1111,
                        word: zext(casted)
                    )
                }
            }
        },
        State::SendByte$(byte_mask, word) => if uart_ready {
            if byte_mask == 0b1000 {
                State::SendNewline
            } else {
                State::SendByte$(byte_mask: byte_mask << 1, word: word << 8)
            }
        } else {
            state
        },
        State::SendNewline => {
            if uart_ready {
                State::SwitchNext
            } else {
                state
            }
        }
    };

    let transmit = match state {
        State::SwitchNext => None,
        State::SendByte$(byte_mask: _, word) => Some(trunc(word >> 24)),
        State::SendNewline => Some(0xA)
    };

    let uart_out = inst uart_tx(clk, rst, transmit, uart_config);

    let uart_ready = uart_out.ready;
    set tx = uart_out.tx;

    scanchain
}
