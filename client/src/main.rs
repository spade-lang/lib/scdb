use std::time::Duration;

use camino::Utf8PathBuf;
use clap::Parser;
use color_eyre::{
    eyre::{anyhow, Context},
    Result,
};
use num::ToPrimitive;
use serial2::SerialPort;
use spade::compiler_state::CompilerState;
use spade_common::location_info::WithLocation;
use spade_hir_lowering::MirLowerable;
use vcd::Value;
use vcd_translate::translation::translate_value;

#[derive(Parser)]
struct Args {
    port: Utf8PathBuf,
    spade_state: Utf8PathBuf,
    /// The spade top module which is being built
    top: String,
    /// The path at which the controller is instantiated
    controller_hierarchy: String,
}

fn main() -> Result<()> {
    color_eyre::install()?;

    let args = Args::parse();

    let mut port = SerialPort::open(args.port, 115200).context("Failed to open serial port")?;
    port.set_read_timeout(Duration::MAX)
        .context("Failed to set timeout")?;

    let spade_state = std::fs::read_to_string(&args.spade_state)
        .with_context(|| format!("Failed to read {}", &args.spade_state))?;

    let ron_ = ron::options::Options::default().without_recursion_limit();
    let compiler_state: CompilerState = ron_
        .from_str(&spade_state)
        .with_context(|| format!("failed to decode compiler state in {:?}", args.spade_state))?;

    let top = compiler_state
        .symtab
        .symtab()
        .lookup_id(
            &spade_common::name::Path::from_strs(
                &args.top.split("::").map(|s| s).collect::<Vec<_>>(),
            )
            .nowhere(),
        )
        .map_err(|e| anyhow!("Failed to find to module {}. {e:?}", args.top))?;

    let controller_witness_path = args
        .controller_hierarchy
        .split(".")
        .map(|s| s.to_string())
        .chain(["look_for_this".to_string()])
        .collect::<Vec<_>>();
    let ty = compiler_state
        .type_of_hierarchical_value(&top, &controller_witness_path)
        .with_context(|| format!("Failed to get type of {controller_witness_path:?}"))?;

    println!("Value is of type {ty}");

    let mut read_queue: Vec<u8> = vec![];
    loop {
        let mut buffer = [0; 256];
        let num = port
            .read(&mut buffer)
            .context("Failed to read from serial port")?;

        read_queue.extend(&buffer[..num]);

        let packets = read_queue
            .split(|b| *b == b'\n')
            .map(|p| p.to_vec())
            .collect::<Vec<_>>();
        read_queue.clear();
        for packet in packets {
            if packet.len() == 4 {
                let value = packet
                    .iter()
                    .flat_map(|p| {
                        let s = format!("{p:08b}");
                        s.chars()
                            .map(|c| if c == '0' { Value::V0 } else { Value::V1 })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>();

                let start = 32 - ty.to_mir_type().size().to_usize().unwrap();
                let val = translate_value(&ty, &value[start..]);

                println!("{val}")
            } else {
                read_queue.extend(packet)
            }
        }
    }
}
